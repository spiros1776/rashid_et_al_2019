#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 19:56:39 2019

@author: spiros
"""

import pickle, sys
import numpy as np
import matplotlib.pyplot as plt

from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis, LinearDiscriminantAnalysis

# Control conditions - somatic recordings
fname1='soma'
fname2='heatmaps'
fname3='control'

fname = fname1+'_'+fname2+'_'+fname3

fname4='No_PCA'

fname_all = fname4+'_' + fname
print ('Current case simulated... ' + fname_all)



# prepare models
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('QDA', QuadraticDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))
models.append(('RF',RandomForestClassifier()))
models.append(('ANN', MLPClassifier(solver='sgd', alpha=0.1, learning_rate='adaptive',
                              max_iter=1000, shuffle=True, early_stopping=True, validation_fraction=0.05, batch_size=100)))
models.append(('ADB', AdaBoostClassifier()))

fg = '_old'
fd = '_'+sys.argv[1]

names = []
nSplits = 5
for iii in range(nSplits):
    split=iii

    with open('algorithms/'+fname+fg+fd+'_f1_score_split'+str(split)+'.pkl', 'rb') as f:
        results = pickle.load(f, encoding='latin1')

    # evaluate each model in turn
    results1 = []
    results2 = []

    for name in results.keys():
        if (name!='test_set') or (name!='train_set'):
            cv_results = []
            for i in range(5):
                cv_results.append(results[name]['all']['split'+str(i)+'_test_score'][results[name]['index']])

            cv_results = np.array(cv_results)
            results1.append(cv_results)

            if iii==0:
                names.append(name)

            results2.append(results[name]['best_result'])

            msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())

            print(msg)
    if iii==0:
        res = np.array(results1)
    else:
        res = np.concatenate((res, np.array(results1)),axis=1)

results1 = list(res)
# boxplot algorithm comparison
names[4]='DT'
plt.rcParams.update({'font.size': 22})
fig = plt.figure()
fig.suptitle('Model Selection')
ax = fig.add_subplot(111)
plt.boxplot(results1)
ax.set_xticklabels(names)
plt.xlabel('Classifiers')
plt.ylabel('f1 score')
plt.grid()
plt.show()
