#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 14:12:13 2019

@author: spiros
"""

import sys, pickle, time, os
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis, LinearDiscriminantAnalysis

from sklearn.model_selection import GridSearchCV

from my_functions import downsample

fname2='heatmaps'
fname1='soma'

# Control conditions
fname3='control'

fname = fname1+'_'+fname2+'_'+fname3
print (fname)

fg='_old'

if int(sys.argv[1])==0:
    fd = '_non_norm'
elif int(sys.argv[1])==1:
    fd = '_smoothed'

with open('python_datasets/'+fname+fg+fd+'.pkl', 'rb') as f:
    data = pickle.load(f, encoding='latin1')

X = data['F']
y = data['y']

fname4='No_PCA'

fname_all = fname4+'_' + fname
print ('Current case simulated... ' + fname_all)

# prepare models
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('QDA', QuadraticDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))
models.append(('RF',RandomForestClassifier()))
models.append(('ANN', MLPClassifier(solver='sgd', alpha=0.1, learning_rate='adaptive',
                              max_iter=1000, shuffle=True, early_stopping=True, validation_fraction=0.05, batch_size=100)))
models.append(('ADB', AdaBoostClassifier()))


grid_params = {}

grid_params['LR'] = {
    'penalty': ['l2'],
    'C': [0.1, 0.5, 1.0, 2.0, 5.0],
    'class_weight': ['balanced'],
    'solver': ['lbfgs']
}

grid_params['LDA'] = {
    'solver': ['svd','lsqr', 'eigen']
}

grid_params['QDA'] = {
    'reg_param': [0.0,0.1,0.2,0.5,1.0],
    'tol': [1.0e-4, 1.0e-6],
}

grid_params['KNN'] = {
    'n_neighbors': [3,5,11,15,21,27,31,51,101],
    'metric': ['manhattan', 'sqeuclidean'],
}

grid_params['CART'] = {
    'splitter': ['best', 'random'],
    'criterion': ['gini', 'entropy'],
    'max_features': ['auto', 'sqrt', 'log2']
}


grid_params['NB'] = {
    'var_smoothing': [1.0e-9, 1.0e-12]
}


grid_params['SVM'] = {
    'C': [0.1, 1, 10, 100, 1000],
    'kernel': ['rbf'],
    'gamma': [1, 0.1, 0.01, 0.001, 0.0001]
}


grid_params['RF'] = {
    'n_estimators': [100, 300, 500, 800, 1000],
    'criterion': ['gini', 'entropy'],
    'bootstrap': [True, False]
}

grid_params['ANN'] = {
    'hidden_layer_sizes': [(100,), (200, ), (200, 100)],
    'activation': ['relu', 'logistic', 'tanh'],
    'learning_rate_init': [0.5,0.2,0.1,0.05]
}

grid_params['ADB'] = {
    'n_estimators': [50,100,300,500,800,1000],
    'learning_rate': [1.0, 0.5, 0.1],
    'algorithm': ['SAMME', 'SAMME.R']
}


t1 = time.time()

for splits in range(5):

    Xd, yd = downsample(X, y, seed=splits)
    X_train, X_test, y_train, y_test = train_test_split(Xd,
                                                        yd,
                                                        test_size = 0.20,
                                                        stratify=yd,
                                                        random_state=splits,
                                                        )


    results = {}
    # scoring = 'roc_auc'
    for name, classifier in models:
        print ('\nChecking Algorithm: '+name)
        gd_sr = GridSearchCV(estimator=classifier,
                             param_grid=grid_params[name],
                             scoring='f1_weighted',
                             cv=5,
                             n_jobs=-1)
        try:
            gd_sr.fit(X_train, y_train)
            best_parameters = gd_sr.best_params_
            print(best_parameters)

            best_result = gd_sr.best_score_
            print(best_result)

            all_results = gd_sr.cv_results_

            best_idx = gd_sr.best_index_
            results[name] = {'best_params': best_parameters,
                             'best_result': best_result,
                             'all': all_results,
                             'index': best_idx}
        except:
            print("\nAn exception occurred.\n")

    dirname='algorithms/'
    os.system('mkdir -p '+dirname)
    with open(dirname+fname+fg+fd+'_f1_score_split'+str(splits)+'.pkl', 'wb') as f:
        pickle.dump(results, f, protocol=-1)

    data_set = {}
    data_set['test_set'] = {'X_train': X_train,'y_train': y_train,
                            'X_test': X_test, 'y_test': y_test}

    with open(dirname+fname+fg+fd+'_data_set_split_'+str(splits)+'.pkl', 'wb') as f:
        pickle.dump(data_set, f, protocol=-1)

t2 = time.time()
tot_time = t2 - t1
print ('\nGrid Search computational time: '+str(np.round(tot_time, 3))+' seconds')
