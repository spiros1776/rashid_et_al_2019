#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 17:49:09 2019

@author: spiros
"""

import sys
import numpy as np
from scipy import stats
from statsmodels.stats.multitest import multipletests
import pandas as pd

label = '_old_'+sys.argv[1]+'_auc'
folder = 'data_finalSubmission/No_PCA'

fname1 = ['soma', 'dendrites']
fname2 = ['heatmaps']
fname3 = [ 'control', 'novel']
i = fname2[0]

algo='ANN'
pca='No_PCA'

filename_fig = 'figures/'+fname2[0]+'_'+algo+'_'+pca

DATA_ALL = []
pval_W = []
df = {}
shapiro = []
wilcoxon = []
pval_S = []

for j in fname1:
    mean = []
    std = []
    for k in fname3:
        filename = folder+'_'+j+'_'+i+'_'+k+label+'.txt'
        DATA = np.loadtxt(filename)

        df[j+'_'+k] = DATA

        rank, p = stats.wilcoxon(DATA-0.50, alternative='greater')
        pval_W.append(p)
        wilcoxon.append([rank,p])

        W, pval = stats.shapiro(DATA)
        pval_S.append(pval)
        shapiro.append([W, pval])

        DATA_ALL.append(DATA)
        print (j+'_'+k)
        print ('Mean ',str(np.round(np.mean(DATA), 2)), ' STD ', str(np.round(np.std(DATA), 3)))
        print(' ')

p_adj1 = multipletests(pval_W, method='bonferroni', alpha=0.001)
p_adj2 = multipletests(pval_S, method='bonferroni', alpha=0.001)

Wilcoxon = {}
Wilcoxon['rank'] = [i[0] for i in wilcoxon]
Wilcoxon['p-value unadj.'] = [i[1] for i in wilcoxon]
Wilcoxon['p-value adj.'] = p_adj1[1]
Wilcoxon = pd.DataFrame(Wilcoxon)
print('Wilcoxon test...\n')
print(Wilcoxon)
Shapiro = {}
Shapiro['W'] = [i[0] for i in shapiro]
Shapiro['p-value unadj.'] = [i[1] for i in shapiro]
Shapiro['p-value adj.'] = p_adj2[1]
Shapiro = pd.DataFrame(Shapiro, index=['soma_fam', 'dend_fam', 'soma_novel', 'dend_novel'])

stat, pValues = stats.kruskal(DATA_ALL[0], DATA_ALL[1], DATA_ALL[2], DATA_ALL[3])
print('\nKruskal-Wallis test...\n')
print(stat, pValues)


statU = np.zeros((4,4))*np.NaN
pVal = np.zeros((4,4))*np.NaN
pVals = []
statUs = []
for ii in range(4):
    for jj in range(4):
        if ii!=jj and ii<jj:
            # print (ii, jj)
            statU[ii,jj], pVal[ii,jj] = stats.mannwhitneyu(DATA_ALL[ii],
                 DATA_ALL[jj],
                 alternative='two-sided')
            pVals.append(pVal[ii,jj])
            statUs.append(statU[ii,jj])

print('\nMann-Whitney test...\n')
print(statUs, pVals)
p_adjusted = multipletests(pVals, method='bonferroni', alpha=0.001)
print('\nBonferoni correction...\n')
print(p_adjusted[1])



# Data for importing in R
AUC = []
Condition = []
for j in fname1:
    for k in fname3:
        AUC += list(df[j+'_'+k])
        for iii in range(len(df[j+'_'+k])):
            Condition.append(j+'_'+k)

df1 = {}
df1['AUC'] = AUC
df1['Condition'] = Condition

df1 = pd.DataFrame(df1)
df1.to_csv(folder+'_all_'+i+'.csv', index=False, header=True)

