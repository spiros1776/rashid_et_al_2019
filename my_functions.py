#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 14:31:47 2018

@author: spiros
"""


def basu_barplots(means, stds, cases, groups, title,filename_fig):
    import matplotlib.pyplot as plt
    import numpy as np
    N = len(means[0])

    fig, ax = plt.subplots(figsize=(12,8))

    ind   = np.arange(N)
    width = 0.20

    colors = ['r', 'g', 'b', 'y']
    for i in range(len(means)):
        plt.bar(ind+(width)*i,
                means[i],
                width,
                color=colors[i],
                edgecolor='k',
                linewidth=2,
                alpha=0.2,
                yerr=stds[i],
                label=groups[i],capsize=12)

    # Set the y axis label
    ax.set_ylabel('AUC score', fontsize=20)
    ax.set_title(title, fontsize=22)
    ax.set_xticks(ind+width*(int(len(means)-1)/2))
    ax.set_xticklabels(cases, fontsize=20)

    ax1 = plt.gca()
    ax1.tick_params(axis = 'both', which = 'major', labelsize = 14)
    ax1.tick_params(axis = 'both', which = 'minor', labelsize = 8)
    ax1.set_facecolor('yellow')
    ax1.patch.set_alpha(0.1)

    m1 = -width*(len(means)-1)
    m2 = ind[-1]+width*(len(means))
    plt.hlines(y=0.5,
               xmin=m1, xmax=m2,
               colors='blue',
               linestyle='dashed',
               linewidth=2.5)
    plt.ylim([0.4, 1.0] )
    plt.legend(groups, fontsize=16)
    plt.grid(which='both',linestyle='--', linewidth=1)
    plt.savefig(filename_fig+'.eps',dpi=1200, format='eps')
    plt.savefig(filename_fig+'.png',dpi=1200, format='png')
    plt.savefig(filename_fig+'.pdf',dpi=1200, format='pdf')
    plt.show()


def downsample(X, y, seed):
    import numpy as np
    from sklearn.utils import resample, shuffle
    L1 = sum(y==0)
    L2 = sum(y==1)

    if L1 > L2:
        X_majority = X[y==0]
        X_minority = X[y==1]
    else:
        X_majority = X[y==1]
        X_minority = X[y==0]

    # Downsample majority class
    X_majority_downsampled = resample(X_majority,
                                      replace=False,    # sample without replacement
                                      n_samples=len(X_minority),# to match minority class
                                      random_state=1000+seed) # reproducible results

    # Combine minority class with downsampled majority class
    X_downsampled = np.concatenate([X_majority_downsampled, X_minority])
    y_downsampled = np.concatenate([np.zeros(X_majority_downsampled.shape[0]), np.ones(X_minority.shape[0])])
    y_downsampled = y_downsampled.astype(int)

    X, y = shuffle(X_downsampled, y_downsampled, random_state=1000+seed)


    X, y = shuffle(X, y, random_state=1000+seed)

    return X, y


