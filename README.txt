
##################################################################################
# Grid search algorithm to find the best classifier based on an average f1 score

python classifier_Ca_imaging_new.py arg1

# arg1=0: non-normalised data
# arg1=1: smoothed data with a gaussian filter


python check_algorithms.py arg1 

# arg1=_non_norm: non-normalised data
# arg1=_smoothed: smoothed data with a gaussian filter
##################################################################################


#################################################################################
# Test best algorithm for all conditions
# 100 random splits in 80-20% training and test, respectively

python classifier_Ca_imaging_new_ALL.py arg1 arg2 arg3

# arg1=0: Familiar Environment
# arg1=1: Novel Environment

# arg2=0: Somatic data
# arg2=1: Dendritic data

# arg3=non_norm: non-normalised data
# arg3=smoothed: smoothed data with a gaussian filter                   
#################################################################################	


#################################################################################
# Barplot of the analysis

python analysis_bars_only_heatmaps.py arg1

# arg1=non_norm: non-normalised data
# arg1=smoothed: smoothed data with a gaussian filter
#################################################################################


#################################################################################
# Statistics                                                                    

python statistics_.py arg1

# arg1=non_norm: non-normalised data 
# arg1=smoothed: smoothed data with a gaussian filter

# For ANOVA use of GraphPad Psism sofware (see the *.xml file) 
#################################################################################


# Figures are saved in figures/ folder, and also the meta-data are saved in a csv file format for usage in any other sofware.





